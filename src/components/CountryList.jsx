// import React from 'react';
import { useCities } from '../context/CitiesContext';
import CountryItem from './CountryItem';
import styles from './CountryList.module.css';
import Message from './Message';
import Spinner from './Spinner';
const CountryList = () => {
  const { cities, loading } = useCities();
  if (loading) {
    return <Spinner />;
  }

  if (!cities.length) {
    return (
      <Message message="Add your very first city by clicking on the map" />
    );
  }

  const countries = cities.reduce((arr, city) => {
    if (!arr.map((el) => el.country).includes(city.country)) {
      return [
        ...arr,
        {
          country: city.country,
          emoji: city.emoji,
        },
      ];
    } else {
      return arr;
    }
  }, []);

  return (
    <ul className={styles.countryList}>
      {countries.map((country) => {
        return <CountryItem country={country} key={country.country} />;
      })}
    </ul>
  );
};

export default CountryList;
